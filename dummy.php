<?php
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
$arr1 =  json_encode('{
  "Siri": {
    "ServiceDelivery": {
      "ResponseTimestamp": "2015-07-21T22:08:15+05:30",
      "ProducerRef": "CTB",
      "StopMonitoringDelivery": [
        {
          "ResponseTimestamp": "2015-07-21T22:08:15+05:30",
          "ValidUntil": "2015-07-21T22:10:15+05:30",
          "MonitoredStopVisit": [
            {
              "RecordedAtTime": "2015-07-11T22:08:11+05:30",
              "MonitoringRef": "20001",
              "MonitoredVehicleJourney": {
                "DirectionRef": "Vishrantwadi to Kothrud",
                "VariantRef": "up",
                "PublishedLineName": "BRT",
                "OperatorRef": "UNKNOWN",
                "ProductCategoryRef": "",
                "ServiceFeatureRef": "",
                "OriginRef": "20030",
                "OriginName": "Kothrud Terminal",
                "DestinationRef": "20000",
                "DestinationName": "Vishrantwadi Terminal",
                "Monitored": false,
                "VehicleLocation": {
                  "Longitude": 0,
                  "Latitude": 0
                },
                "Occupancy": "Seats Available",
                "VehicleRef": "MH14 CW2278",
                "PreviousCalls": [
                  {
                    "PreviousCall": {
                      "StopPointRef": "20002",
                      "VisitNumber": "1",
                      "StopPointName": "Bharati Nagar",
                      "VehicleAtStop": false,
                      "AimedDepartureTime": "2015-07-21T22:29:22+05:30",
                      "ActualDepartureTime": ""
                    }
                  }
                ],
                "MonitoredCall": {
                  "VisitNumber": "1",
                  "Order": "31",
                  "VehicleAtStop": false,
                  "StopPointName": "Vishrantwadi Depot",
                  "VehicleLocationAtStop": {
                    "Longitude": 73.7946996,
                    "Latitude": 18.5076278
                  },
                  "AimedArrivalTime": "2015-07-21T22:27:37+05:30",
                  "ExpectedArrivalTime": "2015-10-15T20:09:00+05:30",
                  "AimedDepartureTime": "2015-07-21T22:28:10+05:30",
                  "ExpectedDepartureTime": "2015-07-21T22:28:10+05:30"
                }
              }
            },
            {
              "RecordedAtTime": "2015-07-21T22:08:15+05:30",
              "MonitoringRef": "20001",
              "MonitoredVehicleJourney": {
                "DirectionRef": "Vishrantwadi to Kothrud",
                "VariantRef": "up",
                "PublishedLineName": "BRT",
                "OperatorRef": "UNKNOWN",
                "ProductCategoryRef": "",
                "ServiceFeatureRef": "",
                "OriginRef": "20030",
                "OriginName": "Vishrantwadi Terminal",
                "DestinationRef": "20000",
                "DestinationName": "Vishrantwadi Terminal",
                "Monitored": false,
                "VehicleLocation": {
                  "Longitude": 0,
                  "Latitude": 0
                },
                "Occupancy": "Seats Available",
                "VehicleRef": "MH14 CW2278",
                "PreviousCalls": [
                  {
                    "PreviousCall": {
                      "StopPointRef": "20002",
                      "VisitNumber": "1",
                      "StopPointName": "Bharati Nagar",
                      "VehicleAtStop": false,
                      "AimedDepartureTime": "2015-07-21T22:27:22+05:30",
                      "ActualDepartureTime": ""
                    }
                  }
                ],
                "MonitoredCall": {
                  "VisitNumber": "1",
                  "Order": "31",
                  "VehicleAtStop": false,
                  "StopPointName": "Kothrud Depot",
                  "VehicleLocationAtStop": {
                    "Longitude": 73.7946996,
                    "Latitude": 18.5076278
                  },
                  "AimedArrivalTime": "2015-07-21T22:27:37+05:30",
                  "ExpectedArrivalTime": "2015-08-27T20:09:00+05:30",
                  "AimedDepartureTime": "2015-07-21T22:28:10+05:30",
                  "ExpectedDepartureTime": "2015-07-21T22:28:10+05:30"
                }
              }
            }
          ]
        }
      ]
    }
  }
}');
$arr2 = json_decode($arr1);
// sleep(55);
echo $arr2;
?>